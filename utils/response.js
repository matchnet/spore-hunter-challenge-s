module.exports = {
    success(body) {
        return buildResponse(200, body);
    },

    failure(body) {
        return buildResponse(500, body);
    },
    notAllowed(body) {
        return buildResponse(406, body);
    }
};

const buildResponse = (statusCode, body) => {
    return {
        statusCode: statusCode,
        body: body
    };
};