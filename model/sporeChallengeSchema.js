const mongoose = require("mongoose");
const userModel = require('./user');

const score = new mongoose.Schema({

    playerId: { type: mongoose.Schema.ObjectId, ref: 'user', required: true },
    //walletId: { type: String, require: true },
    score: { type: String, default: '0' },
    //firstName: { type: String, default: '' },
    //lastName: { type: String, default: '' }

})

const sporeChallengeSchema = new mongoose.Schema({
    title: { type: String, default: '' },
    noOfPlayer: { type: Number, require: true, default: 0 },
    noOfTurns: { type: Number, default: 1 },
    remainingPlayer: { type: Number, default: 0, min: 0 },
    challengeFee: { type: Number, required: true, default: 0 },
    gameName: { type: String, required: true },
    creator: { type: mongoose.Schema.ObjectId, ref: 'user', required: true },
    winAmount: { type: Number, required: true, default: 0 },
    challengeType: { type: String, default: 'High Score' },
    scores: [score],
    highScore: { type: Number, default: 0.1 },
    winner: [{ type: mongoose.Schema.ObjectId, ref: 'user', default: null }],
    challengers: [{
        player: { type: mongoose.Schema.ObjectId, ref: 'user', default: null },
        isPlayed: { type: Boolean, default: false }
    }],
    status: { type: String, enum: ['open', 'start', 'close'], default: 'open' },
    created_on: { type: Date },
    updated_on: { type: Date }
});

const sporeChallenge = (module.exports = mongoose.model("sporeChallenge", sporeChallengeSchema));


// var userOptions = {
//     path: 'score.playerId',
//     select: " fname lname email age walletid ImageUrl _id",
// };

/**
 *  Include Pagination for below function
 * get all challenges as per conditional data
 */
sporeChallenge.getAllChallenges = async(conditionObj, size, pageNo) => {
    if (size && pageNo) {
        return await paginatedGetAllChallenges(conditionObj, size, pageNo);
    } else {
        return new Promise((resolve, reject) => {
            sporeChallenge.find(conditionObj).populate({ path: "winner" }).populate({ path: "scores.playerId" })
                .populate({ path: "creator" }).populate({ path: "challengers.player" }).sort('-created_on')
                .then((docs) => {
                    if (!docs) {

                        resolve(false);
                    } else {

                        resolve({ challenges: docs });
                    }
                }, error => {
                    console.log("error in get all challenges", error);
                    reject(error);
                });
        })
    }


}

const paginatedGetAllChallenges = (conditionObj, size, pageNo) => {
    return new Promise((resolve, reject) => {
        sporeChallenge.countDocuments(conditionObj).then((count) => {
            size = parseInt(size);
            pageNo = parseInt(size * (pageNo - 1));
            sporeChallenge.find(conditionObj).limit(size).skip(pageNo)
                .populate({ path: "winner" }).populate({ path: "scores.playerId" })
                .populate({ path: "creator" }).populate({ path: "challengers.player" }).sort('-created_on')
                .then((docs) => {
                    if (!docs) {

                        resolve(false);
                    } else {

                        resolve({ challenges: docs, pageCount: Math.ceil(count / size) });
                    }
                }, error => {
                    console.log("error in get all challenges", error);
                    reject(error);
                });
        }, error => {
            reject(error);
        });
    })
}

/**
 * create new challenge
 * @param challengeData // new challenge object
 */
sporeChallenge.saveDetails = (challengeData) => {

    return new Promise((resolve, reject) => {
        sporeChallenge.create(challengeData).then((res) => {
            sporeChallenge.findOne({ _id: res._id })
                .populate({ path: "creator" }).populate({ path: "challengers.player" })
                .then((result) => {
                    if (!result) {
                        resolve(false);
                    } else {
                        resolve(result);
                    }
                }, error => {
                    console.log("error in findOne while saving");
                    reject(error);
                })

        }, error => {
            console.log("error in saveDetails", error);
            reject(error);
        })
    })

}

/**
 * returns all finished challenges count
 */
sporeChallenge.getChallengePlayed = () => {
    return new Promise((resolve, reject) => {
        sporeChallenge.countDocuments({ "status": "close" }).then((count) => {
            if (!count) {
                resolve(false);
            } else {
                resolve({ challengeCount: count });
            }
        }, error => {
            console.log("error in get challenge played", error);
            reject(error);
        })
    })

}

/**
 * return the challenge by challenge mongoose Id
 */
sporeChallenge.getChallengeById = (challengeId) => {

        return new Promise((resolve, reject) => {
            sporeChallenge.findOne({ _id: mongoose.Types.ObjectId(challengeId) })
                .populate({ path: "winner" }).populate({ path: "scores.playerId" })
                .populate({ path: "creator" }).populate({ path: "challengers.player" })
                .then((data) => {
                    if (!data) {
                        resolve(false);
                    } else {
                        resolve(data);
                    }
                }, error => {
                    console.log("error in get challenge by id", error);
                    reject(error);
                });
        })

    }
    /**
     * return auto executed challenges 
     */
sporeChallenge.getChallengeForSchedule = (ChallengeId) => {
    return new Promise((resolve, reject) => {
        sporeChallenge.findOne({ _id: ChallengeId }).populate({ path: "winner" }).populate({ path: "scores.playerId" }).populate({ path: "creator" })
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in get challenge for schedule");
                reject(error);
            });
    })
}

/**
 * returns the challenges created by player
 * @param playerId // player mongoose id
 */
sporeChallenge.getChallengeBycreator = (playerId) => {
    var winnerOptions = {
        path: 'winner',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var userOptions = {
        path: 'score.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    return new Promise((resolve, reject) => {
        sporeChallenge.find({ "creator": playerId }).populate(userOptions).populate(creatorOptions).populate(winnerOptions)
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in get challenge by creator", error);
                reject(error);
            });
    })

}

/**
 * returns the challenges accepted or created by the player
 * @param playerId
 */
sporeChallenge.getChallengeByPlayerId = (playerId) => {
    var userOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    return new Promise((resolve, reject) => {
        sporeChallenge.find({ "$and": [{ "selectedSlot.playerId": playerId }, { "creator": { "$ne": playerId } }] }).populate(userOptions)
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in get challenge by player Id", error);
                reject(error);
            });
    })

}

/**
 * return challenges by "status" // open , close, start
 * @param status
 */
sporeChallenge.getChallengeByStatus = (status) => {
    var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var userOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    return new Promise((resolve, reject) => {
        sporeChallenge.find({ status: status }).populate(creatorOptions).populate(userOptions)
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in get challenge by status", error);
                reject(error);
            });
    })

}

/**
 * returns challenges on some conditional data
 * with pagination
 */
sporeChallenge.getChallengeData = (page, perPage, condData = {}) => {
    var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winner',
        select: " fname lname email age walletid username ImageUrl _id",
    };
    var playerOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid username ImageUrl _id",
    };
    return new Promise((resolve, reject) => {
        sporeChallenge.find(condData).populate(creatorOptions).populate(winnerOptions).populate(playerOptions).sort('-created_on').skip((perPage * page) - perPage).limit(perPage)
            .then((challenge) => {
                if (!challenge) {
                    resolve(false);
                } else {
                    sporeChallenge.estimatedDocumentCount(condData).then((count) => {
                        if (!count) {
                            resolve(false);
                        } else {
                            resolve({
                                challenge: challenge,
                                current: page,
                                pages: Math.ceil(count / perPage)
                            });
                        }
                    }, error => {
                        console.log("error in estimated document count", error);
                        reject(error);
                    })
                }
            }, error => {
                console.log("error in get challenge data conddata", error);
                reject(error);
            });
    })

}

/**
 * update challenge 
 * @param id
 * @param status
 * @param jsonData
 */
sporeChallenge.updateChallenge = (id, scoreObj) => {

    query = {
        '_id': id,
        'challengers': {
            $elemMatch: { player: scoreObj.playerId, 'isPlayed': false }
        }
    };
    update = {
        $push: { scores: scoreObj },
        $set: { 'challengers.$.isPlayed': true }
    };
    return new Promise((resolve, reject) => {
        sporeChallenge.findOneAndUpdate(query, update, { returnOriginal: false })
            .populate({ path: "winner" }).populate({ path: "scores.playerId" })
            .populate({ path: "creator" }).populate({ path: "challengers.player" })
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in update challenge", error);
                reject(error);
            });
    })

}

/**
 * update Winner 
 * @param id
 * @param winner
 * @param highScore
 * @param status
 */
sporeChallenge.updateWinner = (id, winner, highScore, status) => {

    let winnerArr = [];
    winner.forEach(element => {
        winnerArr.push(mongoose.Types.ObjectId(element))
    });
    var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winner',
        select: " fname lname email age walletid ImageUrl _id",
    };

    query = { '_id': mongoose.Types.ObjectId(id) },
        update = {
            $set: { winner: winnerArr, status: status, highScore: highScore }
        };
    return new Promise((resolve, reject) => {
        sporeChallenge.findOneAndUpdate(query, update, { new: true })
            .populate(creatorOptions).populate(winnerOptions)
            .populate({ path: "scores.playerId" }).populate({ path: "challengers.player" })
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in updateWinner", error);
                reject(error);
            });
    })

}

/**
 * returns challege with updated status
 * @param id // challenge id
 */
sporeChallenge.updateStatus = (id, status) => {
    return new Promise((resolve, reject) => {
        query = { '_id': id },
            update = {
                $set: { status: status }
            };
        sporeChallenge.findOneAndUpdate(query, update, { new: true }).populate({ path: "creator" })
            .then((data) => {
                if (!data) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            }, error => {
                console.log("error in update status", error);
                reject(error);
            });
    });
}

sporeChallenge.deleteAll = () => {
    return new Promise((resolve, reject) => {
        sporeChallenge.remove({}, function(err, res) {
            if (err) {
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}



/**
 * returns all bowling challenges paginated 
 * @param page
 * @param perPage
 * 
 */
// sporeChallenge.getAllSkillChallenge = (page, perPage, condData = {}) => {
//     return new Promise((resolve, reject) => {
//         sporeChallenge.find(condData).populate({ path: "winner" }).populate({ path: "scores.playerId" }).populate({ path: "creator" }).sort('-created_on').sort('-created_on').skip((perPage * page) - perPage).limit(perPage)
//             .then((challenge) => {
//                 if (!challenge) {
//                     resolve(false);
//                 } else {
//                     sporeChallenge.estimatedDocumentCount(condData).then((count) => {
//                         if (!count) {
//                             resolve(false);
//                         } else {
//                             resolve({
//                                 challenge: challenge,
//                                 current: page,
//                                 pages: Math.ceil(count / perPage),
//                                 count: count
//                             });
//                         }
//                     }, error => {
//                         console.log("error in estimate document count of get all skill challenge", error);
//                         reject(error);
//                     })
//                 }
//             }, error => {
//                 console.log("error in get all skill challenges", error);
//                 reject(error);
//             });
//     })

// }

/**
 * update challenger in the challenge
 * @param id // challenge Id
 * @param playerId
 */
sporeChallenge.updateChallenger = (id, playerId, status) => {
    return new Promise((resolve, reject) => {
        let challenger = {
            player: playerId,
            isPlayed: false
        }
        let updateData = {
            $push: { challengers: challenger },
            //$inc: { remainingPlayer: -1 }
        }
        if (status) {
            updateData = {
                $push: { challengers: challenger },
                //$inc: { remainingPlayer: -1 },
                $set: { status: status }
            }
        }
        sporeChallenge.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id), status: 'open' }, updateData, { returnOriginal: false })
            .populate({ path: "winner" }).populate({ path: "scores.playerId" })
            .populate({ path: "creator" }).populate({ path: "challengers.player" })
            .then((challenge) => {
                if (!challenge) {
                    resolve(false);
                }
                resolve(challenge);
            }, error => {
                console.log("error in update Challenger");
                reject(error);
            })
    })
}

sporeChallenge.updateChallengeForPreAndCancelBook = (id, playerId, isBook) => {
    return new Promise((resolve, reject) => {


        let updateData = {
            $inc: { remainingPlayer: -1 }
        }
        if (!isBook || isBook == false) {
            updateData = {
                $inc: { remainingPlayer: 1 }
            }
        }
        //console.log(updateData);
        sporeChallenge.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id), status: 'open' }, updateData, { returnOriginal: false })
            .then((challenge) => {
                if (!challenge) {
                    resolve(false);
                }
                //console.log("pre and cancel book ", challenge);
                resolve(challenge);
            }, error => {
                console.log("error in update Challenger", error);
                reject(error);
            })
    })
}